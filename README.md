# Yo

Yo is an ExpressJS application using this container runtime: `registry.gitlab.com/bots-garden/funky/funky-node-js-runtime:latest`.

The details of **funky-node-js-runtime** is here: [https://gitlab.com/bots-garden/funky/funky-node-js-runtime/container_registry/](https://gitlab.com/bots-garden/funky/funky-node-js-runtime/container_registry/)

## How to deploy your application

```bash
export KUBECONFIG=path_to_your_kubernetes_config_file
export SUB_DOMAIN="X.X.X.X.nip.io" # or whathever you want
./deploy
```

> - you can override the `RUNTIME_IMAGE` variable
>   - the default value is `registry.gitlab.com/bots-garden/funky/funky-node-js-runtime:latest`
> - you can override the `NAMESPACE` variable
>   - the default value is `funky-apps-${BRANCH}` where `${BRANCH}` is the current branch
 